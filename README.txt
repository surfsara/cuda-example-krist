wv 2010-02-16

In this directory you find an example program that uses CUDA for
the acceleration of the computation of structure factors.

The file cuda-structurefactors.odf and the corresponding pdf 
contain a short explanation how to program in the CUDA environment
and short description of the algorithm that is optimized.

Compilation of the program on the lisa system:

  - copy this directory to the lisa system
  - login to gpu.sara.nl  ( gpu.sara.nl shares it's home file system
                            with lisa.sara.nl )
  - change directory to this directory
  - enter:
    module load cuda
    make
    ./krist

