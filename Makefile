CC	= gcc
NVCC	= nvcc
LD	= $(CC)
CFLAGS	= -Wall -O3
LIBS	= -lcudart

NVCCPARMS += -use_fast_math

PROG=krist

OBJS =  krist.o custructfac.o clocks.o

all: $(PROG)
	
$(PROG):	$(OBJS)
	$(LD) -o $@ $(OBJS) $(LIBS)
	

NVCCINC=-I $(CUDASDK)/common/inc

.SUFFIXES:

%.o:	%.c
	$(CC) $(CFLAGS) -o $@ -c $<

%.o:	%.cu
	$(NVCC) $(NVCCPARMS) -o $@ -c $< 

clean:
	rm -f *.o $(PROG) .depend *.linkinfo

include .depend

.depend dep:
	gcc -M *.c > .depend
	nvcc $(NVCCINC) $(NVCCPARMS) -M *.cu >> .depend
	
test:
	@echo NVCCPARMS: $(NVCCPARMS)
