#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "custructfac.h"
#include "clocks.h"
void structfac(int na, int nr, float*a, float*h, float*E);
void deta(int na, float*a);
void deth(int nr, float*h);

int main(int argc, char*argv[])
{
   int na=1000;   /* number of atoms */
   int nr=10000; /* number of reflections */
 
   if (argc > 1)
   {
     na = atoi(argv[1]);
     nr = atoi(argv[2]);
   }
   
   printf("Computation of crystallographic normalized structure factors\n"
          "                on the CPU and the GPU\n\n");
   printf("Number of atoms:       %d\n",na);
   printf("Number of reflections: %d\n",nr);

   float *h;  /* h[j,0] == h, h[j,1] == k, h[j,2] == l */
   float *E;  /* E[j,0] == real part of E, E[j,1] == imag part of E */
   float *E1;  /* E[j,0] == real part of E, E[j,1] == imag part of E */
   float *a;  /* a[j,0] == atomic number, a[j,1] == x, a[j,2] == y,
                 a[j,3] == z */
   float *v;

   double t0,dt1,dt2;
   int i;

   h = (float*) malloc(sizeof(*h)*3*nr);
   E = (float*) malloc(sizeof(*E)*2*nr);
   E1 = (float*) malloc(sizeof(*E)*2*nr);
   a = (float*) malloc(sizeof(*a)*4*na);
   v = (float*) malloc(sizeof(*v)*1000);

   // determine a values:
   deta(na,a);

   // determine h values:
   deth(nr,h);

   // running the reference function structfac:
   
   t0=wallclock();
   structfac(na,nr,a,h,E);
   dt1 = wallclock() - t0;

   printf("\nCPU: wallclock time: %f seconds\n",dt1);

   // running the gpu code:
   int times=100;
   printf("\nRunning the gpu code %d times\n",times);
   custructfac(na,nr,a,h,E1,&dt2,v,times);

   printf("\nCuda: wallclock time: %f seconds\n",dt2);
   printf("Cuda: speedup vs CPU: %f\n",dt1/dt2);

   // checking the result:

   printf("\nChecking the result ...\n");
   double sumdif = 0;
   for (i=0; i<2*nr; i++)
     sumdif += fabsf(E[i] - E1[i]);

   printf("Differences between CPU and GPU versions:\n"
      "Sum:  %f\n"
      "Mean: %f\n",sumdif,sumdif/nr);

   printf("\nDebugging output:\n");
   for (i=0; i<4; i++)
     printf("%f\n",v[i]);
   return 0;
   
}

// the reference code. In the .cu file is the gpu version:

void structfac(int na, int nr, float*a, float*h, float*E)
{
  int i,j;
  float A,B,twopi;
  twopi = 6.28318584f;

  float f2 = 0.0f;
  for (i=0; i<na; i++)
     f2 += a[4*i]*a[4*i];
  f2 = 1.0f/sqrtf(f2);
    
  for (i=0; i<nr; i++)
  {
    A=0.0f;
    B=0.0f;
    for (j=0; j<na; j++)
    {
      float A1,B1;
      float arg = twopi*(h[3*i+0]*a[4*j+1] +
                         h[3*i+1]*a[4*j+2] +
                         h[3*i+2]*a[4*j+3]);
      sincosf(arg, &B1, &A1);
      A += a[4*j]*A1;
      B += a[4*j]*B1;
    }
    E[2*i]   = A*f2;
    E[2*i+1] = B*f2; 
  }
}

// produce random values for the atomic numbers and
// coordinates
void deta(int na, float*a)
{
   int i,j;
   for(i=0; i<na; i++)
   {
     if (i & 1)
       a[4*i] = 6.0;
     else
       a[4*i] = 7.0;
      for (j=1; j<4; j++)
      a[4*i+j] = (float)random()/(float)RAND_MAX;
   }
}

// produce random numbers for the h-array
// notice: these values are in prnciple integer, but for performance
// reasons, they are represented as float in this program
void deth(int nr, float*h)
{
   const int hmax=20;
   const int kmax=30;
   const int lmax=15;
   int i;
   for(i=0; i<nr; i++)
   {
      h[3*i+0] = rintf(2*hmax*(float)random()/(float)RAND_MAX - hmax);
      h[3*i+1] = rintf(2*kmax*(float)random()/(float)RAND_MAX - kmax);
      h[3*i+2] = rintf(2*lmax*(float)random()/(float)RAND_MAX - lmax);
   }
}

