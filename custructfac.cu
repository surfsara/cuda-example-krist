#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "custructfac.h"
#include "clocks.h"

// this is the gpu version of structfac() in krist.c 
// essentially, the outer i-loop is removed, every i value
// is covered by one thread.
//
__global__ void cstructfac(int na, int nr, float*a, float*h, 
              float*E, float*v)
{
  // get the i-value of this thread:
  int i = blockDim.x * blockIdx.x + threadIdx.x;
  if (i < nr) // the number of threads is in general somewhat
              // larger than nr, so we have to test
  {
     int j;
     float A,B,twopi;
     twopi = 6.28318584f;

     float f2=0.0f;
     A = 0.0f;
     B = 0.0f;
     for (j=0; j<na; j++)
     {
        float A1,B1;
        f2 += a[4*j]*a[4*j];
        float arg = twopi*(h[3*i+0]*a[4*j+1] +
                           h[3*i+1]*a[4*j+2] +
                           h[3*i+2]*a[4*j+3]);
        sincosf(arg, &B1, &A1);
        A += a[4*j]*A1;
        B += a[4*j]*B1;
     }
     f2       = 1.0f/sqrtf(f2);
     E[2*i  ] = A*f2;
     E[2*i+1] = B*f2;
  }
}

// this is the code to transport data to and from the gpu and
// start a sufficient number of threads, each running 
// the function cstructfac:
//
extern "C" void custructfac(int na, int nr, float*a, float*h, 
                            float*E, double *time, float*v, int times)
// the arrray v is used to return in v[0] the number of seconds the
// computations took. The array can also be used as a kind of
// debugging aid: cstructfac could put some values in it.
//
{
   float *deva,*devh,*devE,*devv;
   int r;

   // we use 256 threads per block and compute the number of blocks
   // needed
   //
   dim3 threads(256);
   int blocks = nr/threads.x;
   if (blocks*threads.x < nr)
      blocks++;
   dim3 grid(blocks);

   printf("blocks, threads: %d %d\n",grid.x, threads.x);

   // allocate devh (copy of h) on the gpu:
   //
   r = cudaMalloc((void**)&devh,sizeof(*h)*nr*3);
   if (r != 0)
     {
        printf("Cannot allocate memory for h on device\n");
        exit(1);
     }
   cudaMemcpy(devh,h,sizeof(*h)*nr*3,cudaMemcpyHostToDevice);

   // allocate devE (copy of E) on the gpu:
   //
   r = cudaMalloc((void**)&devE,sizeof(*E)*nr*2);
   if (r != 0)
     {
        printf("Cannot allocate memory for E on device\n");
        exit(1);
     }

   // allocate devv (v on the cpu) on the gpu:
   //
   r = cudaMalloc((void**)&devv,sizeof(*v)*1000);
   if (r != 0)
     {
        printf("Cannot allocate memory for v on device\n");
        exit(1);
     }
   // zero devv:
   //
   cudaMemset(devv,0,1000*sizeof(*v));

   // call cstructfac, inclusive the copying of
   // data a number of times:
   // In real life: the array h is fixed, so that one is copied to
   // the gpu once. The array a will change between calls, so
   // we copy that every time.
   double t0 = wallclock();
   for (int tt = 0; tt < times; tt++)
   {
     r = cudaMalloc((void**)&deva,sizeof(*a)*na*4);
     if (r != 0)
       {
	  printf("Cannot allocate memory for a on device\n");
	  exit(1);
       }
     cudaMemcpy(deva,a,sizeof(*a)*na*4,cudaMemcpyHostToDevice);

     cstructfac<<<grid, threads>>> (na,nr,deva,devh,devE,devv);

     //CUT_CHECK_ERROR("Kernel execution failed");
     cudaError_t err = cudaGetLastError();                                    \
     if( cudaSuccess != err) 
     {
       printf("Kernel execution failed\n");
       exit(1);
     }

     cudaMemcpy(E,devE,sizeof(*E)*nr*2,cudaMemcpyDeviceToHost);

     // free the memory occupied by deva on the gpu:
     //
     cudaFree(deva);
   }
   double dt = (wallclock() - t0)/times;

   // copy devv to v:
   //
   cudaMemcpy(v,devv,sizeof(*v)*1000,cudaMemcpyDeviceToHost);
   // store walltime in time:
   *time=dt;
}

// vim: set filetype=c :
